# coding: utf-8
# 
# Scripts d'exemple per el TFG Anàlisi de l'assignació de canals en GuifiSants
# (c) Llorenç Cerdà-Alabern. Gener 2020
#
# https://ruby-doc.org/stdlib-2.6.3/libdoc/strscan/rdoc/StringScanner.html
# https://ruby-doc.org/stdlib-2.4.0/libdoc/zlib/rdoc/Zlib/GzipReader.html
# https://ruby-doc.org/stdlib-2.5.3/libdoc/yaml/rdoc/YAML.html
# https://yaml.org/YAML_for_ruby.html

require "zlib"
require "strscan"
require "pry-remote"
require 'pry-nav'

##
## stores the raw capture of a node
##
class NodeRawCapture
  attr_accessor :header, :content

  def initialize(sec_line)
    @header = sec_line.scan(/\S+/)
    @content = {}
  end

  def show_keys
    @content.each {|key, value| 
      puts key
    }
  end

  def print_id
    puts "# node " + @header.join(" ")
  end

  def print
    print_id
    @content.each {|key, value| 
      show_content(key, value)
    }
  end

  def print_key(key)
    print_id
    if @content[key]
      show_content(key, @content[key])
    else
      puts key + "?"
    end
  end

  ## private
  def show_content(key, value, pref="#")
    if value.is_a?(Hash)
      puts "#{pref} #{key} is"
      value.each {|k, v| 
        show_content(k, v, pref + "#")
      }
    else
      puts "#{pref} #{key} is #{value}"
    end
  end
  private :show_content
end # NodeRawCapture

##
## create an array of node captures
##
class GuifisantsRawParser
  attr_reader :nodes, :date

  def initialize(zfile)
    @nodes = []
    parse_trace(zfile)
  end

  def print
    nodes.each{|n|
      n.print
    }
  end

  ## private
  def parse_trace(zfile)
    infile = open(zfile)
    gz = Zlib::GzipReader.new(infile, options = {:encoding => 'iso-8859-1'})
    trace_cont = gz.read
    buffer = StringScanner.new(trace_cont)
    if buffer.scan(/^# date /)
      @date = buffer.scan_until(/$/)
    end
    if buffer.skip_until(/^# start /)
      while read_node(buffer)
      end
    end
  end

  def merge_section(sections, key, is_subsec, next_pat, str)
    if(is_subsec) # there are subsections
      sections.merge!(key => read_sections(StringScanner.new(str), next_pat))
    else
      sections.merge!(key => str)
    end
  end

  def read_sections(data, pat)
    sections = {}
    str_pat = "#{pat} start "
    regex_pat = Regexp.new("^#{str_pat}")
    next_pat = "#{pat}#"
    regex_next_pat = Regexp.new("^#{next_pat} start ")
    data.skip_until(regex_pat)
    while true
      sec_line = data.scan_until(/$/)
      head = sec_line.scan(/\S+/)
      key = head[0..-2].join(" ")
      if(head)
        if(res = data.scan_until(regex_pat))
          merge_section(sections, key, regex_next_pat =~ res, next_pat, res.chomp(str_pat))
        else
          merge_section(sections, key, regex_next_pat =~ res, next_pat, data.rest)
          break
        end
      else
        break
      end
    end
    return sections
  end

  def read_node(buffer)
    sec_line = buffer.scan_until(/$/)
    if(sec_line && (sec_line.length > 0))
      @nodes << NodeRawCapture.new(sec_line)
      if(res = buffer.scan_until(/^# start /))
        @nodes.last.content = read_sections(StringScanner.new(res), "##")
        return true
      else
        @nodes.last.content = read_sections(StringScanner.new(buffer.rest), "##")
      end
    end
    return false
  end
  private :parse_trace, :read_sections, :merge_section, :read_node
end # GuifisantsRawParser

# Local Variables:
# mode: ruby
# coding: utf-8
# End: 
