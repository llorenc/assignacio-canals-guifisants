#!/usr/bin/ruby -w
# coding: utf-8
# 
# Scripts d'exemple per el TFG Anàlisi de l'assignació de canals en GuifiSants
# (c) Llorenç Cerdà-Alabern. Gener 2020
#
# https://ruby-doc.org/stdlib-2.5.1/libdoc/optparse/rdoc/OptionParser.html

require 'optparse'

require_relative 'guifisants-raw-parser-lib.rb'
require_relative 'guifisants-parser-lib.rb'

Options = Struct.new(:name)

class Optparse
  class ScriptOptions
    attr_accessor :node, :name, :section, :list, :raw

    def initialize
      @node = nil
      @name = nil
      @trace = nil
      @section = nil
      @list = nil
    end

    def define_options(parser)
      parser.banner = "Usage: " + File.basename($0) + " [options] trace"
      # add additional options
      parser.on("-nNODE", "--node=NODE", "only NODE (-1 for last)") do |n|
        @node = n
      end
      parser.on("-aNAME", "--name=NAME", "only NODE with NAME") do |n|
        @name = n
      end
      parser.on("-sSEC", "--section=SEC", "list section SEC") do |n|
        @section = n
      end
      parser.on("-r", "--raw", "raw trace") do |n|
        @raw = true
      end
      parser.on("-l", "--list", "list section names") do |n|
        @list = true
      end
      parser.on("-h", "--help", "Prints this help") do
        puts parser
        exit
      end
      parser.on_tail("-h", "--help", "Show this message") do
        puts parser
        exit
      end
    end
  end
  #
  # Return a structure describing the options.
  #
  def parse(args)
    # The options specified on the command line will be collected in *options*.
    @options = ScriptOptions.new
    @args = OptionParser.new do |parser|
      @options.define_options(parser)
      parser.parse!(args)
    end
    @options
  end
  
  attr_reader :parser, :options
end  # class Optparse

defopts = Optparse.new
opts = defopts.parse(ARGV)

if ARGV.length != 1
  puts "trace? (" + File.basename($0) + " -h for help)"
  exit 1
end

tfile = ARGV[0]

if opts.raw
  trace = GuifisantsRawParser.new(tfile)
else
  trace = GuifisantsParser.new(tfile)
end

if opts.node
  if opts.node.to_i < trace.nodes.length
    trace = trace.nodes[opts.node.to_i]
  else
    puts "Error: #{opts.node} >= number of nodes (#{trace.nodes.length})"
    exit 1
  end
end

if opts.name
  found = false
  trace.nodes.each{|n|
    if(opts.raw)
      if(n.content["hostname"].match?(opts.name))
        trace = n
        found = true
        break
      end
    else
      if(n.name == opts.name)
        trace = n
        found = true
        break
      end
    end
  }
  if !found
    puts "Error: #{opts.name} not found"
    exit 1
  end
end

if opts.list
  if !opts.node
    trace = trace.nodes[0]
  end
  trace.show_keys
  exit
end

if opts.raw && opts.section
  if !opts.node
    trace = trace.nodes[0]
  end
  trace.print_key(opts.section)
  exit
end

trace.print

# Local Variables:
# mode: ruby
# coding: utf-8
# End: 
