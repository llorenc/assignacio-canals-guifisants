# coding: utf-8
# 
# Scripts d'exemple per el TFG Anàlisi de l'assignació de canals en GuifiSants
# (c) Llorenç Cerdà-Alabern. Gener 2020
#
# https://ruby-doc.org/stdlib-2.6.3/libdoc/strscan/rdoc/StringScanner.html
# https://ruby-doc.org/stdlib-2.4.0/libdoc/zlib/rdoc/Zlib/GzipReader.html
# https://ruby-doc.org/stdlib-2.5.3/libdoc/yaml/rdoc/YAML.html
# https://yaml.org/YAML_for_ruby.html

require "zlib"
require "strscan"
require 'pry'
require 'yaml'

require_relative 'guifisants-raw-parser-lib.rb'

## BSSID de la mesh de Sants
$GuifiSantsBSSID = '02:ca:ff:ee:ba:be'

class String
  def unquote
    return self.gsub(/\A["']|["']\Z/, '')
  end
  def unquote!
    self.gsub!(/\A["']|["']\Z/, '')
  end
end

class Array
  def unquote
    self.each{|s|
      s.unquote
    }
  end
  def unquote!
    self.each{|s|
      s.unquote!
    }
  end
end

class ParseField
  def get_field(data, f)
    res = data.match("#{f}: (\\S+)")
    return res.captures[0].unquote if res
    return nil
  end

  def get_int(data, f)
    res = get_field(data, f)
    return res.to_i if res
    return nil
  end

  def get_float(data, f)
    res = get_field(data, f)
    return res.to_f if res
    return nil
  end
end

##
## Stores the iwscan
##
class Iwscan < ParseField
  attr_reader :scan_list

  def initialize(val)
    @scan_list = parse_iwscan(val)
  end

  def parse_bss(data)
    fields = {}
    ssid = get_field(data, "SSID")
    if(ssid)
      if((res = get_float(data, "signal")) && (res != 0)) # discard self node scanning
        fields.merge!({"signal" => res})
      else
        return nil
      end
      if(res = get_int(data, "primary channel"))
        fields.merge!({"channel" => res})
      else
        return nil
      end
    else
      return nil
    end
    return {ssid => fields}
  end

  def add_bss_data(sections, data)
    bss = parse_bss(data)
    if(bss) 
      sections.merge!(bss)
    end
  end
  
  def parse_iwscan(data)
    sections = {}
    str_pat = "BSS "
    regex_pat = Regexp.new("^#{str_pat}")
    data.skip_until(regex_pat)
    while true
      sec_line = data.scan_until(/$/)
      head = sec_line.scan(/..:..:..:..:..:../)
      mac = head[0]
      if(head)
        if(res = data.scan_until(regex_pat))
          if(mac == $GuifiSantsBSSID)
            add_bss_data(sections, res.chomp(str_pat))
          end
        else
          if(mac == $GuifiSantsBSSID)
            add_bss_data(sections, data.rest)
          end
          break
        end
      else
        break
      end
    end
    return sections
  end
end # Iwscan

class Iwinfo < ParseField
  attr_reader :info_list

  def initialize(iwinfo)
    @info_list = {"ssid" => get_field(iwinfo, 'ESSID'),
                  "mode" => get_field(iwinfo, 'Mode'),
                  "channel" => get_int(iwinfo, 'Channel'),
                  "txpower" => get_float(iwinfo, 'Power')}
  end
end # Iwinfo

class ReadWirelessConfig
  attr_reader :sections

  def initialize(rawsection)
    @sections = {}
    read_sections(StringScanner.new(rawsection))
  end

  def get_ssid
    ssid = {}
    if sections['wifi-device']
      sections['wifi-device'].each {|r, ro|
        if ro['channel']
          if sections['wifi-iface']
            sections['wifi-iface'].each {|i, io|
              if io['device'] && (io['device'] == r)
                if io['ifname']
                  ifname = io['ifname'].unquote
                else
                  ifname = r.unquote
                  ifname = ifname.sub(/radio/, 'wlan')
                end
                if io['ssid']
                  ssid.merge!(ifname => {'ssid' => io['ssid'].unquote, 'channel' => ro['channel'].unquote.to_i})
                elsif io['mesh_id']
                  ssid.merge!(ifname => {'meshid' => io['mesh_id'], 'channel' => ro['channel'].unquote.to_i})
                end
                break
              end
            }
          end
        end
      }
    end
    return ssid
  end

  # private
  def read_options(data)
    options = {}
    str_pat = "option "
    regex_pat = Regexp.new("#{str_pat}")
    while data.exist?(regex_pat)
      data.skip_until(regex_pat)
      sec_line = data.scan_until(/$/)
      head = sec_line.scan(/\S+/).unquote
      if(head && (head.length > 1))
        options.merge!(head[0] => head[1])
      end
    end
    return options
  end

  def add_section(k1, k2, str)
    v = read_options(StringScanner.new(str))
    if @sections[k1]
      @sections[k1].merge!(k2 => v)
    else
      @sections.merge!(k1 => { k2 => v })
    end
  end
  
  def read_sections(data)
    str_pat = "config "
    regex_pat = Regexp.new("^#{str_pat}")
    data.skip_until(regex_pat)
    while true
      sec_line = data.scan_until(/$/)
      head = sec_line.scan(/\S+/).unquote
      if(head && head.length > 1)
        if(res = data.scan_until(regex_pat))
          add_section(head[0], head[1], res.chomp(str_pat))
        else
          add_section(head[0], head[1], data.rest)
          break
        end
      else
        break
      end
    end
  end
  private :read_options, :add_section, :read_sections
end # ReadWirelessConfig

##
## stores the capture of a node
##
class NodeCapture
  attr_reader :ipv6, :name, :date, :time
  attr_reader :iwinfo, :iwscan, :wconfig

  def initialize(node_raw_capture)
    # puts "node: " + @name
    initialize_nodeid(node_raw_capture.header)
    initialize_iwscan(node_raw_capture.content)
    initialize_iwinfo(node_raw_capture.content)
    initialize_wconfig(node_raw_capture.content)
  end

  def print
    $stdout.write(self.to_yaml)
  end

  # private
  def initialize_nodeid(head)
    @ipv6, @name, @date, @time = head[0], head[1][1..-2], head[2], head[3]
  end

  def read_iwinfo_option(data, k)
    if match = data.match("#{k}: (\\S+)")
      return  match.captures[0].unquote
    end
  end

  def initialize_iwscan(content)
    @iwscan = {}
    key = "iwscan"
    if content[key] && content[key].is_a?(Hash)      
      content[key].each {|k, v| 
        scan = Iwscan.new(StringScanner.new(v))
        if(scan.scan_list && (scan.scan_list.length > 0))
          wlan = k.sub(/iwscan /, '')
          @iwscan.merge!(wlan => scan.scan_list)
        end
      }
    end
  end

  def initialize_iwinfo(content)
    @iwinfo = {}
    key = "iwinfo"
    if content[key] && content[key].is_a?(Hash)      
      content[key].each {|k, v| 
        info = Iwinfo.new(v)
        if(info.info_list && (info.info_list.length > 0))
          wlan = k.sub(/iwinfo /, '')
          @iwinfo.merge!(wlan => info.info_list)
        end
      }
    end
  end

  def initialize_wconfig(content)
    @wconfig = {}
    key = "wirelessconfig"
    if content[key]
      wc = ReadWirelessConfig.new(content["wirelessconfig"])
      @wconfig = wc.get_ssid
    end
  end

  private :initialize_nodeid, :initialize_iwscan, :initialize_iwinfo, :initialize_wconfig
end # NodeCapture

##
## create an array of node captures
##
class GuifisantsParser
  attr_reader :nodes, :date

  def initialize(zfile)
    @nodes = []
    trace = GuifisantsRawParser.new(zfile)
    @date = trace.date
    trace.nodes.each{|n|
      @nodes << NodeCapture.new(n)
    }
  end

  def print
    $stdout.write(self.to_yaml)
  end
end

# Local Variables:
# mode: ruby
# coding: utf-8
# End: 
