#!/usr/bin/env bash
# 
# Scripts d'exemple per el TFG Anàlisi de l'assignació de canals en GuifiSants
# (c) Llorenç Cerdà-Alabern. Gener 2020

traced="traces-mesh"
yamld="traces-yaml"

if [ ! -d "$traced" ] ; then
    echo "$traced?"
    exit 1
fi

if [ ! -d "$yamld" ] ; then
    echo "mkdir $yamld"
    mkdir "$yamld"
fi

for f in $traced/*.log.gz
do
    yname=$(basename $f "-qmpnodes-gather-info-perl-openssh.log.gz").yml
    # ls -al $f
    
    echo "building $yamld/$yname"
    ./guifisants-parser.rb $f > $yamld/$yname
done
